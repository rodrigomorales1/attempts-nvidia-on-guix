Module                  Size  Used by
nvidia_uvm           1191936  0
ipmi_devintf           20480  0
ipmi_msghandler        69632  1 ipmi_devintf
nvidia_drm             61440  2
nvidia_modeset       1155072  3 nvidia_drm
intel_rapl_msr         20480  0
intel_rapl_common      32768  1 intel_rapl_msr
edac_mce_amd           36864  0
kvm_amd               147456  0
nvidia              40976384  93 nvidia_uvm,nvidia_modeset
snd_hda_codec_realtek   159744  1
ucsi_ccg               20480  0
typec_ucsi             45056  1 ucsi_ccg
snd_hda_codec_generic    81920  1 snd_hda_codec_realtek
kvm                  1019904  1 kvm_amd
ledtrig_audio          16384  1 snd_hda_codec_generic
deflate                16384  1
snd_hda_codec_hdmi     69632  1
typec                  86016  1 typec_ucsi
wmi_bmof               16384  0
irqbypass              16384  1 kvm
crct10dif_pclmul       16384  1
crc32_pclmul           16384  0
snd_hda_intel          53248  0
polyval_clmulni        16384  0
polyval_generic        16384  1 polyval_clmulni
snd_intel_dspcfg       36864  1 snd_hda_intel
snd_intel_sdw_acpi     20480  1 snd_intel_dspcfg
ghash_clmulni_intel    16384  0
drm_kms_helper        184320  1 nvidia_drm
snd_hda_codec         159744  4 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec_realtek
sha512_ssse3           45056  0
input_leds             16384  0
aesni_intel           393216  0
snd_hda_core          106496  5 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek
snd_hwdep              20480  1 snd_hda_codec
drm                   544768  6 drm_kms_helper,nvidia,nvidia_drm
snd_pcm               139264  4 snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_core
crypto_simd            16384  1 aesni_intel
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
fb_sys_fops            16384  1 drm_kms_helper
syscopyarea            16384  1 drm_kms_helper
snd_timer              40960  1 snd_pcm
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
r8169                  86016  0
snd                   110592  8 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_pcm
pcspkr                 16384  0
efi_pstore             16384  0
soundcore              16384  1 snd
k10temp                16384  0
sp5100_tco             20480  0
realtek                32768  1
i2c_piix4              28672  0
ccp                   106496  1 kvm_amd
i2c_nvidia_gpu         16384  0
i2c_ccgx_ucsi          16384  1 i2c_nvidia_gpu
gpio_amdpt             20480  0
wmi                    32768  1 wmi_bmof
gpio_generic           20480  1 gpio_amdpt
mac_hid                16384  0
virtio_rng             16384  0
virtio_console         32768  0
virtio_net             65536  0
virtio_blk             24576  0
virtio_balloon         28672  0
virtio_pci             24576  0
virtio                 20480  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
virtio_pci_legacy_dev    16384  1 virtio_pci
virtio_pci_modern_dev    20480  1 virtio_pci
virtio_ring            40960  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
isci                  151552  0
libsas                 98304  1 isci
scsi_transport_sas     45056  2 isci,libsas
pata_atiixp            16384  0
pata_acpi              16384  0
nls_iso8859_1          16384  1
wp512                  36864  0
serpent_generic        28672  0
xts                    16384  0
dm_crypt               53248  0
hid_apple              24576  0
hid_generic            16384  0
usbhid                 57344  0
hid                   147456  3 usbhid,hid_apple,hid_generic
uas                    28672  0
usb_storage            77824  1 uas
ahci                   49152  0
libahci                45056  1 ahci

Module                  Size  Used by
snd_seq_dummy          16384  0
snd_seq                73728  1 snd_seq_dummy
snd_seq_device         16384  1 snd_seq
intel_rapl_msr         20480  0
intel_rapl_common      28672  1 intel_rapl_msr
nouveau              2134016  2
edac_mce_amd           36864  0
mxm_wmi                16384  1 nouveau
video                  61440  1 nouveau
drm_ttm_helper         16384  1 nouveau
snd_hda_codec_realtek   159744  1
ttm                    77824  2 drm_ttm_helper,nouveau
ucsi_ccg               20480  0
kvm_amd               147456  0
snd_hda_codec_generic    86016  1 snd_hda_codec_realtek
typec_ucsi             45056  1 ucsi_ccg
ledtrig_audio          16384  1 snd_hda_codec_generic
snd_hda_codec_hdmi     69632  1
typec                  86016  1 typec_ucsi
wmi_bmof               16384  0
drm_display_helper    151552  1 nouveau
snd_hda_intel          53248  0
snd_intel_dspcfg       36864  1 snd_hda_intel
drm_kms_helper        180224  2 drm_display_helper,nouveau
kvm                  1007616  1 kvm_amd
snd_intel_sdw_acpi     20480  1 snd_intel_dspcfg
deflate                16384  1
snd_hda_codec         159744  4 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec_realtek
irqbypass              16384  1 kvm
drm                   536576  7 drm_kms_helper,drm_display_helper,drm_ttm_helper,ttm,nouveau
crct10dif_pclmul       16384  1
snd_hda_core          106496  5 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek
crc32_pclmul           16384  0
polyval_clmulni        16384  0
snd_hwdep              20480  1 snd_hda_codec
polyval_generic        16384  1 polyval_clmulni
snd_pcm               139264  4 snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_core
ghash_clmulni_intel    16384  0
input_leds             16384  0
i2c_algo_bit           16384  1 nouveau
aesni_intel           393216  0
fb_sys_fops            16384  1 drm_kms_helper
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
snd_timer              40960  2 snd_seq,snd_pcm
crypto_simd            16384  1 aesni_intel
sysimgblt              16384  1 drm_kms_helper
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
snd                   110592  10 snd_hda_codec_generic,snd_seq,snd_seq_device,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_pcm
soundcore              16384  1 snd
r8169                  90112  0
pcspkr                 16384  0
sp5100_tco             20480  0
efi_pstore             16384  0
k10temp                16384  0
i2c_piix4              28672  0
realtek                32768  1
i2c_nvidia_gpu         16384  0
ccp                   106496  1 kvm_amd
i2c_ccgx_ucsi          16384  1 i2c_nvidia_gpu
wmi                    32768  3 wmi_bmof,mxm_wmi,nouveau
gpio_amdpt             20480  0
mac_hid                16384  0
gpio_generic           20480  1 gpio_amdpt
virtio_rng             16384  0
virtio_console         32768  0
virtio_net             65536  0
virtio_blk             24576  0
virtio_balloon         28672  0
virtio_pci             24576  0
virtio                 20480  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
virtio_pci_legacy_dev    16384  1 virtio_pci
virtio_pci_modern_dev    20480  1 virtio_pci
virtio_ring            40960  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
isci                  147456  0
libsas                 98304  1 isci
scsi_transport_sas     45056  2 isci,libsas
pata_atiixp            16384  0
pata_acpi              16384  0
nls_iso8859_1          16384  1
wp512                  36864  0
serpent_generic        28672  0
xts                    16384  0
dm_crypt               53248  0
hid_apple              24576  0
hid_generic            16384  0
usbhid                 57344  0
hid                   147456  3 usbhid,hid_apple,hid_generic
uas                    28672  0
usb_storage            77824  1 uas
ahci                   49152  0
libahci                45056  1 ahci

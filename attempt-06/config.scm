(use-modules
 (gnu)
 (nongnu services nvidia)
 (nongnu packages linux)
 (nongnu packages nvidia))

(use-service-modules cups desktop networking ssh xorg)

(operating-system
 (locale "en_GB.utf8")
 (timezone "America/Lima")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "desktop")
 (kernel linux-6.1)
 (kernel-arguments (append
		    '("modprobe.blacklist=nouveau")
		    %default-kernel-arguments))
 (kernel-loadable-modules (list nvidia-module))
 (users (cons* (user-account
                (name "rdrg")
                (comment "Rdrg")
                (group "users")
                (home-directory "/home/rdrg")
                (supplementary-groups '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))
 (packages (append (list (specification->package "emacs")
                         (specification->package "emacs-exwm")
                         (specification->package
                          "emacs-desktop-environment")
                         (specification->package "nss-certs"))
                   %base-packages))
 (services (append (list
		    (service nvidia-service-type)
                    (service openssh-service-type)
                    (set-xorg-configuration
                     (xorg-configuration (keyboard-layout keyboard-layout))))
                   %desktop-services))
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              (keyboard-layout keyboard-layout)))
 (swap-devices (list (swap-space
                      (target (uuid
                               "f95df01e-5e58-4d7a-945d-1e1c4db969d3")))))
 (file-systems (cons* (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "5E5C-90FD"
                                     'fat32))
                       (type "vfat"))
                      (file-system
                       (mount-point "/")
                       (device (uuid
                                "5fdf4f83-3625-4a63-a65b-361da87376f0"
                                'ext4))
                       (type "ext4")) %base-file-systems)))

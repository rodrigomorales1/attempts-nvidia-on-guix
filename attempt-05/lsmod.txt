Module                  Size  Used by
nvidia_drm             61440  1
nvidia_modeset       1155072  1 nvidia_drm
nvidia              40976384  1 nvidia_modeset
intel_rapl_msr         20480  0
intel_rapl_common      32768  1 intel_rapl_msr
edac_mce_amd           36864  0
snd_hda_codec_realtek   159744  1
kvm_amd               147456  0
snd_hda_codec_generic    81920  1 snd_hda_codec_realtek
ledtrig_audio          16384  1 snd_hda_codec_generic
ucsi_ccg               20480  0
snd_hda_codec_hdmi     69632  1
kvm                  1019904  1 kvm_amd
typec_ucsi             45056  1 ucsi_ccg
deflate                16384  1
typec                  86016  1 typec_ucsi
irqbypass              16384  1 kvm
snd_hda_intel          53248  0
snd_intel_dspcfg       36864  1 snd_hda_intel
snd_intel_sdw_acpi     20480  1 snd_intel_dspcfg
crct10dif_pclmul       16384  1
wmi_bmof               16384  0
crc32_pclmul           16384  0
snd_hda_codec         159744  4 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec_realtek
polyval_clmulni        16384  0
drm_kms_helper        184320  1 nvidia_drm
polyval_generic        16384  1 polyval_clmulni
input_leds             16384  0
ghash_clmulni_intel    16384  0
snd_hda_core          106496  5 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek
snd_hwdep              20480  1 snd_hda_codec
sha512_ssse3           45056  0
drm                   544768  5 drm_kms_helper,nvidia,nvidia_drm
snd_pcm               139264  4 snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec,snd_hda_core
aesni_intel           393216  0
fb_sys_fops            16384  1 drm_kms_helper
snd_timer              40960  1 snd_pcm
syscopyarea            16384  1 drm_kms_helper
sysfillrect            16384  1 drm_kms_helper
sysimgblt              16384  1 drm_kms_helper
snd                   110592  8 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_pcm
crypto_simd            16384  1 aesni_intel
r8169                  86016  0
sp5100_tco             20480  0
soundcore              16384  1 snd
cryptd                 24576  2 crypto_simd,ghash_clmulni_intel
pcspkr                 16384  0
k10temp                16384  0
realtek                32768  1
i2c_nvidia_gpu         16384  0
i2c_piix4              28672  0
efi_pstore             16384  0
i2c_ccgx_ucsi          16384  1 i2c_nvidia_gpu
ccp                   106496  1 kvm_amd
gpio_amdpt             20480  0
wmi                    32768  1 wmi_bmof
mac_hid                16384  0
gpio_generic           20480  1 gpio_amdpt
virtio_rng             16384  0
virtio_console         32768  0
virtio_net             65536  0
virtio_blk             24576  0
virtio_balloon         28672  0
virtio_pci             24576  0
virtio                 20480  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
virtio_pci_legacy_dev    16384  1 virtio_pci
virtio_pci_modern_dev    20480  1 virtio_pci
virtio_ring            40960  6 virtio_rng,virtio_console,virtio_balloon,virtio_pci,virtio_blk,virtio_net
isci                  151552  0
libsas                 98304  1 isci
scsi_transport_sas     45056  2 isci,libsas
pata_atiixp            16384  0
pata_acpi              16384  0
nls_iso8859_1          16384  1
wp512                  36864  0
serpent_generic        28672  0
xts                    16384  0
dm_crypt               53248  0
hid_apple              24576  0
hid_generic            16384  0
usbhid                 57344  0
hid                   147456  3 usbhid,hid_apple,hid_generic
uas                    28672  0
usb_storage            77824  1 uas
ahci                   49152  0
libahci                45056  1 ahci
